# TWITCH ME

Authorizing with Twitch and Selecting a Streamer
================================================

The authorization and streamer selection flow is the following:

Before authorization:
--------------------

1. When the user clicks "Log In" button, she/he is sent to **/authorize** endpoint, where:

   - OAuth2 session is created
   - Twitch authorization url is built
   - after that the user is redirected to Twitch authorization url.

2. After authorizing with Twitch the user is redirected to **/callback** endpoint, where:
   - an authorization code included in the request URL is retrieved
   - oauth token is fetched using the auth. code and stored in the user session
   - authorised user is redirected back to the index page.

After authorization:
--------------------

1. On **GET /**:
   - oauth token is checked and refreshed if it expired
   - Twitch users API is called to get the **user's followed channels**
   - index page is returned with followed channels in context.

2. On **POST /**:
   - a form is submitted with **channel-id** field, containing the id of the selected streamer
   - received id is then used to call Twitch users API to get the streamer **name**
   - A redirect to **streamer page** is returned.
   

Subscribing to Streamer Events
==============================

1. On redirect to **/streamer** endpoint, EventListener is initialised, and with 
the help **subscribe** method requests to subscription are sent to Twitch API:
 
    Example request to subscribe to an event:
    
    POST https://api.twitch.tv/helix/webhooks/hub
    ```
        {
            "hub.mode": "subscribe",
            "hub.topic": "https://api.twitch.tv/helix/users/follows?first=1&to_id=1337",
            "hub.callback": "https://twitch-me.herokuapp.com/subscription/callback"
            "hub.lease_seconds": "864000",
            "hub.secret": "s3cRe7"
        }
    ```
    
    Twitch should respont with "202 Accepted" status code.
    
2. Subscription Verify Request (GET) is sent by Twitch to ** /subscription/callback** url. 
    
    Example Twitch request:

    ```
    GET https://twitch-me.herokuapp.com/subscription/callback?hub.mode=subscribe&hub.topic=https://api.twitch.tv/helix/users/follows?first=1&to_id=1337& hub.lease_seconds=864000&hub.challenge=HzSGH_h04Cgl6VbDJm7IyXSNSlrhaLvBi9eft3bw
    ```
    If **hub.mode** query param is **subscribe**, the response should be **"hub.challenge"** from query parameter.
    
    Example response to Twitch:
    
    ```
    HzSGH_h04Cgl6VbDJm7IyXSNSlrhaLvBi9eft3bw
    ```
    
    If **hub.mode** query param is **denied**, an empty response with "200 OK" status code is sent.
 
3. When event occurs on Twitch, it sends a POST request to **/subscription/callback** url. 
The payload is then processed and the message about the event is written to Redis list, 
the name of which represents streamer_id parsed from POST request **Link** header:

    Example header:
    ```
    Link: <https://api.twitch.tv/helix/webhooks/hub>; rel="hub", <https://api.twitch.tv/helix/users/follows?first=1&to_id=27446517>; rel="self"
    ``` 

Getting Event List for a Streamer
=================================

1. When a new event notification from Twitch is processed by Backend

On page load:
-------------

1. When **/streamer** page is loaded, a Websocket connection is set up by Front-end (FE), 
and **streamer_id** event and a message with streamer id is sent to Backend (BE).

2.  On receiving "streamer_id" event, BE replies with **streamer_events** event and a message containing 
a list of ten last Redis records for the requested streamer id.

3. FE displays the list of streamer events got on **streamer_events**.

On notification received from Twitch:
-------------------------------------

1. On Twitch notification **check_updates** event is sent from BE to FE, which responds with a **streamer_id** event.

2. Steps 2 and 3 are same as for the ones on page load.


Deploying on AWS
================

If I needed to deploy that to AWS, I would use AWS Docs:
[Deploying a Flask Application to AWS Elastic Beanstalk](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-flask.html)
and architecture similar to:

![alt text](https://d1.awsstatic.com/Projects/Python/arch_diagram.3edf54fb1c8d9fdec47ff1950c81211798b27d6b.png)

 Hopefully, load balancing, clusterization, orchestration (with Kubernetes, for example) 
 and master-slaving DB would do the trick for scaling up the app. 
 