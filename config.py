import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY', '')
    TWITCH_AUTH_BASE_URL = os.getenv('TWITCH_AUTH_BASE_URL')
    TWITCH_CLIENT_ID = os.getenv('TWITCH_CLIENT_ID')
    TWITCH_SECRET_KEY = os.getenv('TWITCH_SECRET_KEY')
    TWITCH_TOKEN_BASE_URI = os.getenv('TWITCH_TOKEN_BASE_URI')
    REDIS_URL = os.getenv('REDIS_URL', 'redis://localhost:6379')
