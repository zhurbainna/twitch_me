import json
import os
from _sha256 import sha256
from datetime import datetime
from http import HTTPStatus

from flask import url_for, session
from requests_oauthlib import OAuth2Session


TOPIC_MAP = {
    'user_follows': 'https://api.twitch.tv/helix/users/follows?first=1&to_id=%s',
    'stream_changed': 'https://api.twitch.tv/helix/streams?user_id=%s'
}


class EventListener(object):
    """
    A class used for subscribing to Twitch streamer events and process event notifications received from Twitch.
    """
    def __init__(self, streamer_id=None):
        self.streamer_id = streamer_id
        self.client_id = os.getenv('TWITCH_CLIENT_ID')
        self.token = session.get('oauth_token')
        if self.token:
            self.twitch = OAuth2Session(self.client_id, token=self.token)
        self.subscribe_url = 'https://api.twitch.tv/helix/webhooks/hub'

    def subscribe(self, topic, hub_mode='subscribe'):
        """
        Send POST request to twitch to subscribe/unsubscribe to streamer events.

        :param topic: a string with a topic name
        :param hub_mode: a string defining whether the request is for subscription or cancelling it,
                        defaults to "subscribe"
        :return: a response to a sent request. Value error is raised if topic is not supported.
        """
        if self.validate_topic(topic):
            secret = sha256(os.getenv('SECRET_KEY').encode('utf-8'))
            payload_data = {
                "hub.mode": hub_mode,
                "hub.topic": TOPIC_MAP.get(topic) % self.streamer_id,
                "hub.callback": url_for('subscription_callback', _external=True),
                "hub.lease_seconds": "864000",
                "hub.secret": secret
            }
            sub_response = self.twitch.post(self.subscribe_url, data=payload_data)
            if sub_response.status_code == HTTPStatus.ACCEPTED:
                return sub_response
        else:
            raise ValueError('Topic is not supported.')

    @staticmethod
    def validate_topic(topic):
        """
        Check if topic is in the list of supported ones.

        :param topic: a string with a topic name
        :return: bool, True if topic is among supported ones
        """
        return topic in TOPIC_MAP.keys()

    def parse_event_notification(self, request, app_redis):
        """
        On receiving Twitch request, parse request data, create a different message on each type
        of event and push it to Redis.

        :param request: a request object to parse
        :param app_redis: redis client instance
        :return: msg, a string with the message on event or an empty string if event could not be processed
        """
        msg = ''
        data = json.loads(request.data)
        try:
            # A way to make a distinction between streamer subscriptions by parsing Link header from Twitch request .
            link = request.headers.get('link', '')
            link = link.replace('<', '$').replace('>', '$').rsplit('$', 2)[-2]
            self.streamer_id = link.split('=')[-1]
            if self.streamer_id:
                event_timestamp = datetime.now().strftime("%m-%d-%Y, %H:%M:%S")
                if link == TOPIC_MAP.get('user_follows') % self.streamer_id:
                    msg = 'The streamer has a new follower {}'.format(data['data'][0].get('from_name'))
                elif link == TOPIC_MAP.get('stream_changed') % self.streamer_id:
                    if data and data['data'][0].get('type') == 'live':
                        msg = 'The streamer is now live!'
                    elif not data:
                        msg = 'The streamer went offline.'
                if msg:
                    app_redis.rpush(self.streamer_id, '{}:{}'.format(event_timestamp, msg))
        except(TypeError, KeyError, IndexError):
            return msg
        finally:
            return msg
