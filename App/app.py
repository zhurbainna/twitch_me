import json
import logging
import sys
from http import HTTPStatus

import redis
from flask import Flask, session, redirect, request, url_for, render_template, Response
from flask_socketio import SocketIO
from oauthlib.oauth2 import TokenExpiredError
from requests_oauthlib import OAuth2Session

from App.event_listener import EventListener
from config import Config


app = Flask(__name__)
app.config.from_object(Config)
socketio = SocketIO(app)
app_redis = redis.StrictRedis.from_url(app.config.get('REDIS_URL'), 0, decode_responses=True)

app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.DEBUG)


def if_token_expired_refresh():
    """
    A helper function checks if resources can be requested with an old token, if not tries to refresh it.

    :return: void
    """
    client_id = app.config.get('TWITCH_CLIENT_ID')
    twitch_token_base_url = app.config.get('TWITCH_TOKEN_BASE_URI')
    twitch_session = OAuth2Session(client_id, token=session['oauth_token'], auto_refresh_kwargs={
        'client_id': client_id,
        'client_secret': app.config.get('TWITCH_SECRET_KEY')
    })
    try:
        request_not_expired = twitch_session.head('https://api.twitch.tv/helix/users')
        if request_not_expired.status_code == HTTPStatus.UNAUTHORIZED:
            token = twitch_session.refresh_token(twitch_token_base_url)
            session['oauth_token'] = token
    except TokenExpiredError:
        token = twitch_session.refresh_token(twitch_token_base_url)
        session['oauth_token'] = token


@app.route("/", methods=['GET', 'POST'])
def index():
    """
    A route to index page.
    """
    if request.method == 'POST':
        twitch = OAuth2Session(app.config.get('TWITCH_CLIENT_ID'), token=session['oauth_token'])
        favourite_channel_id = request.form.get('channel-id')
        # request Twitch for a streamer's display name by id
        favourite_channel_name = twitch.get(
            'https://api.twitch.tv/helix/users?id=%s' % favourite_channel_id).json().get('data')[0]['display_name']
        return redirect(url_for('streamer', channel_name=favourite_channel_name, channel_id=favourite_channel_id))
    elif request.method == 'GET' and session.get('user_authorized', 0):
        # Checking if the token is the token is still valid and refreshing if not
        if_token_expired_refresh()
        twitch = OAuth2Session(app.config.get('TWITCH_CLIENT_ID'), token=session['oauth_token'])
        # request Twitch for user data
        user_data_response = twitch.get('https://api.twitch.tv/helix/users')
        if user_data_response.status_code == HTTPStatus.UNAUTHORIZED:
            session['user_authorized'] = 0
            return render_template('index.html')
        user_data = user_data_response.json()
        user_id = user_data['data'][0].get('id')
        # request Twitch for user follows
        user_follows = twitch.get(
            'https://api.twitch.tv/helix/users/follows?from_id={from_id}'.format(from_id=user_id)).json().get('data')
        return render_template('index.html', user_follows=user_follows)
    else:
        return render_template('index.html')


@app.route("/authorize", methods=["GET"])
def authorize():
    """
    A route used to redirect the user to Twitch using an URL with required OAuth parameters and set oauth state.

    :return: Redirect to the authorisation URL.
    """
    twitch_auth_base_url = app.config.get('TWITCH_AUTH_BASE_URL')
    redirect_uri = url_for('callback', _external=True)
    twitch = OAuth2Session(app.config.get('TWITCH_CLIENT_ID'), scope=['user:edit', 'user:read:email',
                                                                      'channel:read:subscriptions'],
                           redirect_uri=redirect_uri)
    authorization_url, state = twitch.authorization_url(twitch_auth_base_url, force_verify='true')

    session['oauth_state'] = state
    return redirect(authorization_url)


@app.route("/callback", methods=["GET"])
def callback():
    """
     A route used for retrieving an access token.

    It is a callback URL registered in Twitch. The user is redirected here back after authorizing with the providor.
    With this redirection comes an authorization code included in the request URL.
    From that an access token is obtained.

    :return: Redirect to index.
    """
    redirect_uri = request.base_url
    code = request.args.get('code')
    twitch = OAuth2Session(app.config.get('TWITCH_CLIENT_ID'), state=session['oauth_state'],
                           scope=request.args.get('scope'), redirect_uri=redirect_uri)
    token = twitch.fetch_token(token_url=app.config.get('TWITCH_TOKEN_BASE_URI'),
                               client_secret=app.config.get('TWITCH_SECRET_KEY'),
                               include_client_id=True,
                               code=code)
    session['oauth_token'] = token
    session['user_authorized'] = 1
    return redirect(url_for('index'))


@app.route("/streamer", methods=["GET"])
def streamer():
    """
    A route used to get an embedded live stream, chat and list of the most recent events
    for a selected favourite streamer.

    :return: If user is authorized, she/he is redirected to a streamer page, otherwise to index.
    """
    if session.get('user_authorized', 0):
        channel_name = request.args.get('channel_name')
        channel_id = request.args.get('channel_id')
        session['channel_id'] = channel_id
        try:
            events = EventListener(streamer_id=channel_id)
            events.subscribe('user_follows')
            events.subscribe('stream_changed')
        except ValueError:
            return Response(HTTPStatus.BAD_REQUEST)
        twitch = OAuth2Session(app.config.get('TWITCH_CLIENT_ID'), token=session['oauth_token'])
        return render_template('streamer.html', twitch=twitch, channel_name=channel_name, channel_id=channel_id)
    else:
        return redirect(url_for('index'))


@app.route("/logout", methods=["GET"])
def logout():
    """
    A route used to log out the user, by deleting auth token and user_authorised variable from the session.

    :return: Redirect to index.
    """
    session.clear()
    return redirect(url_for('index'))


@app.route("/subscription/callback", methods=['GET', 'POST'])
def subscription_callback():
    """
    A route used to confirm subscriptions to events for Twitch (if GET) or send event notification
    got from Twitch to connected client.

    :return: Response with 'hub.challenge' text, if called for subscription confirmation,
            or empty Response otherwise.
    """
    if request.method == 'GET':
        app.logger.info("GET /subscription/callback {}".format(request.args))
        hub_mode = request.args.get('hub.mode')
        if hub_mode == 'subscribe':
            return Response(request.args.get('hub.challenge'), status=HTTPStatus.OK)
        elif hub_mode == 'denied':
            return Response(status=HTTPStatus.OK)
    elif request.method == 'POST':
        app.logger.info('POST /subscription/callback DATA {}'.format(json.loads(request.data)))
        app.logger.info('POST /subscription/callback {}'.format(request.headers))
        events = EventListener()
        msg = events.parse_event_notification(request, app_redis)
        if msg:
            socketio.emit('check_updates', namespace='/streamer_ws')
        return Response(status=HTTPStatus.ACCEPTED)


@socketio.on('connect', namespace='/streamer_ws')
def ws_connect():
    """
    Websocket handler for 'connect' event.
    :return: void
    """
    socketio.emit('connected', {'msg': 'connected'}, namespace='/streamer_ws')


@socketio.on('streamer_id', namespace='/streamer_ws')
def streamer_id(message):
    """
    Websocket handler for 'streamer_id' event, takes the id from message and gets last 10 events from Redis.

    :param message: a dict with streamer_id data
    :return: void
    """
    streamer_id = message.get('streamer_id')
    events_list = app_redis.lrange(streamer_id, -10, -1)
    if events_list:
        socketio.emit('streamer_events', {'recent_events': events_list}, namespace='/streamer_ws')


@socketio.on('disconnect', namespace='/streamer_ws')
def ws_disconnect():
    """
    Websocket handler for 'disconnect' event.
    :return: void
    """
    socketio.emit('disconnect', {'msg': 'disconnected'}, namespace='/streamer_ws')


if __name__ == '__main__':
    app.debug = False
    socketio.run(app)
