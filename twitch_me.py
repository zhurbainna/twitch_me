from App.app import app
from App.app import socketio


if __name__ == '__main__':
    app.debug = False
    socketio.run(app)