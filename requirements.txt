Flask==1.0
requests_oauthlib
Flask-SocketIO==3.3.2
redis==3.2.0
eventlet
gunicorn
